# WearDuke - School of Nursing Project

<img src="https://i.imgur.com/ODEgKBg.jpg" >


This is an Android app developed for Duke University, Nursing School, Professor Ryan Shaw. This app is meant to collect user activity data and surevy answers and store it into a file, so that Professor Shaw can check it and maybe upload it in further development. This is an Android application project created by the following students.

- Yifan Men - ym129@duke.edu
- Kevin Wang - kw284@duke.edu
- Liming Jin - lj142@duke.edu

## Features

- This app uses APIs provided by Fitbit Official
- Collect Fitbit user activity data whenever the app is started
- Only in a certain period of time that user can do the survey
- The app will pop out a notification 30 mins on default before the survey is closed

## Prereqs

### Android Studio

Make sure you have the latest version of Android Studio Installed and the latest Android SDK: https://developer.android.com/studio/index.html If you have never used Android Studio before, it should prompt you to download and install an Android SDK the first time you open the app.

### Register for a Fitbit Dev Account 

Go to https://dev.fitbit.com/apps and create and account. You'll need to use a valid email address and confirm it before continuing.

### Create a Fitbit App

In this section, we are going to talk about how to request for an access token from Fitbit.

Step 1: Open https://dev.fitbit.com/apps/new

Step 2: Fill out the fields about your app. The first five fields (app name, description, website, organization, and organization website) will be specific to your organization and will be what users see in the app when giving permissions. 

**Important**:

- For `OAuth 2.0 Application Type` select `Client`
- For `Callback URL` choose a non-url word starting with `https://`. We recommend `https://finished`

Example: 

![Edit Application](https://i.imgur.com/AAQBjYI.png)


Step 3: Agree and click Register. You should get a screen with your `Client Id` and `Client Secret`. Replace [`22DLBT`](https://gitlab.oit.duke.edu/ym129/son/blob/master/app/src/main/AndroidManifest.xml#L21) with your Client ID in Line21 of file `app/src/main/AndroidManifest.xml`. Replace [`22DLBT`](https://gitlab.oit.duke.edu/ym129/son/blob/master/app/src/main/AndroidManifest.xml#L21) with your Client ID in Line21 of file `app/src/main/AndroidManifest.xml`. Replace [`3790b30ce6e4d6d1b67085db16405a42`](https://gitlab.oit.duke.edu/ym129/son/blob/master/app/src/main/java/edu/duke/ece651/spring19/son/fitbittestapi/FitbitAuthApplication.java#L36) with your Client Secret in Line36 of file `app/src/main/java/edu/duke/ece651/spring19/son/fitbittestapi/FitbitAuthApplication.java`.

![](https://i.imgur.com/6mesJhs.jpg)


## Installation


Then you can clone this project to local with 

```
git clone git@gitlab.oit.duke.edu:ym129/son.git
```

Open your android studio, and import this project.

Then navigate to `Run -> Run 'app'`, after a while the app should be opened automatically.

## Test

If you do not have a Andorid device around you, you can create an emulator. We suggest to create Pixel 2 XL with Andorid API 27. This is also the environment that we tested our app.


## Usage

We provide a testing Fitbit account, the username is `kevin420933601@outlook.com`, the password is `123456son`. You can use it as login account. For current version, we can not open survey directly, you need to install another app to be called on [lj142/survey](https://gitlab.oit.duke.edu/lj142/survey).

### Login

When the user opens the app the first time, the app will launch the login page. The user should login with his Fitbit account and grant access to the app.

<img src="https://i.imgur.com/QJsyM1n.png" width="20%" height="20%">

<img src="https://i.imgur.com/BjR52i2.png" width="20%" height="20%">

<img src="https://i.imgur.com/xFiS4pZ.png" width="20%" height="20%">

### Information Demonstration Page

There will be profile information, device information, activity information, weight information, other operations like reminder setting and do survery. 

<img src="https://i.imgur.com/sW7vouc.png" width="20%" height="20%">

<img src="https://i.imgur.com/hHeu28Z.png" width="20%" height="20%">

<img src="https://i.imgur.com/bRjzuSG.png" width="20%" height="20%">
<img src="https://i.imgur.com/lf7vJ7P.png" width="20%" height="20%">
<img src="https://i.imgur.com/NMswKuX.png" width="20%" height="20%">



### Reminder Setting Page

The users could see the setting.

<img src="https://i.imgur.com/XcWSWZx.png" width="20%" height="20%">

### Survey Page

The users could do the surveys after use pressed `do survey` button. When user finishes his survey, the app should jump back to previsou Information Demonstration Page.

<img src="https://i.imgur.com/CVrjAOw.png" width="20%" height="20%">
<img src="https://i.imgur.com/nEV1T9G.png" width="20%" height="20%">
<img src="https://i.imgur.com/bKbSxfy.png" width="20%" height="20%">
<img src="https://i.imgur.com/HzVnmya.png" width="20%" height="20%">
<img src="https://i.imgur.com/dt9Cd2M.png" width="20%" height="20%">


## Acknowlegements

We use the following services or open-source libraries. So we'd like show them highest respect and thank for bringing those great projects:

- [nutes-uepb/simple-survey](https://github.com/nutes-uepb/simple-survey)
- [Stasonis/fitbit-api-example-android](https://github.com/Stasonis/fitbit-api-example-android)

