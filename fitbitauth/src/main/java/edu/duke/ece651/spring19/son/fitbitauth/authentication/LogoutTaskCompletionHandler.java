package edu.duke.ece651.spring19.son.fitbitauth.authentication;

/**
 * Created by SON Team.
 */
public interface LogoutTaskCompletionHandler {
    void logoutSuccess();

    void logoutError(String message);
}
