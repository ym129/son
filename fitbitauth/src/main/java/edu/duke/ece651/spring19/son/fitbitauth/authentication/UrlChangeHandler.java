package edu.duke.ece651.spring19.son.fitbitauth.authentication;

/**
 * Created by SON Team.
 */
public interface UrlChangeHandler {
    void onUrlChanged(String newUrl);
    void onLoadError(int errorCode, CharSequence description);
}
