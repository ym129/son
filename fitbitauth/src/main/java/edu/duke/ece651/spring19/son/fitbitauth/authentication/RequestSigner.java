package edu.duke.ece651.spring19.son.fitbitauth.authentication;


import edu.duke.ece651.spring19.son.fitbitcommon.BasicHttpRequestBuilder;

/**
 * Created by SON Team.
 */

public interface RequestSigner {

    void signRequest(BasicHttpRequestBuilder builder);

}
