package edu.duke.ece651.spring19.son.fitbittestapi.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderResult;
import edu.duke.ece651.spring19.son.fitbitapi.models.UserContainer;
import edu.duke.ece651.spring19.son.fitbitapi.services.UserService;
import edu.duke.ece651.spring19.son.fitbittestapi.R;
import edu.duke.ece651.spring19.son.fitbittestapi.ReminderPage;
import edu.duke.ece651.spring19.son.fitbittestapi.RootActivity;

public class ButtonFragment extends InfoFragment<UserContainer> {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        binding.webview.setVisibility(View.GONE);
        binding.graph.setVisibility(View.GONE);

        Button survey = new Button(getActivity());
        survey.setText("Do survey");
        binding.linearlay1.addView(survey);
        survey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat df = new SimpleDateFormat("HH:mm");//设置日期格式
                Date now =null;
                Date beginTime = null;
                Date endTime = null;
                try {
                    now = df.parse(df.format(new Date()));
                    beginTime = df.parse("14:00");
                    endTime = df.parse("22:00");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Boolean flag = belongCalendar(now, beginTime, endTime);
                System.out.println(flag);
                if(flag) {
                    String packageName = "br.edu.uepb.nutes.simplesurvey.example";
                    //要调用另一个APP的activity名字
                    String activity = "br.edu.uepb.nutes.simplesurvey.example.MainActivity";
                    ComponentName component = new ComponentName(packageName, activity);
                    Intent intent = new Intent();
                    intent.setComponent(component);
                    intent.setFlags(101);
                    startActivity(intent);
                }
                else {
                    new AlertDialog.Builder(getActivity())
//                            .setIcon(R.drawable.fitbit_logo)
                            .setTitle("Can not do survey at this time!")
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            // TODO Auto-generated method stub

                                        }
                                    }).create()
                            .show();
                }


                /*
                Date date =  new Date();
                System.out.println(date);
                try{
                    System.out.println(new SimpleDateFormat("HH:mm").parse("21:00"));
                }
                catch (ParseException e) {

                }
                try{
                    // change time to do the survey
                    if(date.before(new SimpleDateFormat("HH:mm").parse("21:00"))) {
                        String packageName = "br.edu.uepb.nutes.simplesurvey.example";
                        //要调用另一个APP的activity名字
                        String activity = "br.edu.uepb.nutes.simplesurvey.example.MainActivity";
                        ComponentName component = new ComponentName(packageName, activity);
                        Intent intent = new Intent();
                        intent.setComponent(component);
                        intent.setFlags(101);
                        startActivity(intent);
                    }
                }
                catch (ParseException e) {

                }*/
            }
        });


        Button reminder = new Button(getActivity());
        reminder.setText("Reminder setting");
        binding.linearlay1.addView(reminder);
        reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ReminderPage.class);
                startActivity(intent);
            }
        });

        return v;
    }



    @Override
    public int getTitleResourceId() {
        return R.string.others;
    }

    @Override
    protected int getLoaderId() {
        return 5;
    }

    @Override
    public Loader<ResourceLoaderResult<UserContainer>> onCreateLoader(int id, Bundle args) {
        return UserService.getLoggedInUserLoader(getActivity());
    }


    @Override
    public void onLoadFinished(Loader<ResourceLoaderResult<UserContainer>> loader, ResourceLoaderResult<UserContainer> data) {
        super.onLoadFinished(loader, data);

    }


    /**
     * 判断时间是否在时间段内
     * @param nowTime
     * @param beginTime
     * @param endTime
     * @return
     */
    public static boolean belongCalendar(Date nowTime, Date beginTime, Date endTime) {
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }
}
