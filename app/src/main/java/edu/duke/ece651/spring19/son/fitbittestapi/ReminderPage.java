package edu.duke.ece651.spring19.son.fitbittestapi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.Calendar;

public class ReminderPage extends AppCompatActivity {
    private CheckBox[] checkBoxes = new CheckBox[2];
    public static int hour = 21;
    public static int min = 46;
    public static int time_before_finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        checkBoxes[0] = findViewById(R.id.checkBox30min);
        checkBoxes[1] = findViewById(R.id.checkBox1hour);
        checkBoxes[0].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    checkBoxes[0].setChecked(true);
                    checkBoxes[1].setChecked(false);
                }
            }
        });
        checkBoxes[1].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    checkBoxes[1].setChecked(true);
                    checkBoxes[0].setChecked(false);
                }
            }
        });
    }

    public void Confirm(View view) {
        // Do something in response to button
        if (checkBoxes[0].isChecked()) {
            time_before_finish = 30;
        }
        else {
            time_before_finish = 60;
        }
        RootActivity.c.set(Calendar.HOUR_OF_DAY, hour);
        if (time_before_finish == 60) {
            RootActivity.c.set(Calendar.MINUTE, min);
            RootActivity.c.set(Calendar.HOUR_OF_DAY, hour - 1);
        }
        else {
            if (min > time_before_finish) {
                RootActivity.c.set(Calendar.MINUTE, min - time_before_finish);
                RootActivity.c.set(Calendar.HOUR_OF_DAY, hour);
            } else {
                RootActivity.c.set(Calendar.HOUR_OF_DAY, hour - 1);
                RootActivity.c.set(Calendar.MINUTE, 60 - time_before_finish - min);
            }
        }
        System.out.println(time_before_finish);
        RootActivity.c.set(Calendar.SECOND, 0);
        Intent intent1 = new Intent(this, AlertRecv.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,1,intent1,0);
        RootActivity.alarmManager.setExact(AlarmManager.RTC_WAKEUP, RootActivity.c.getTimeInMillis(), pendingIntent);

        Intent intent = new Intent(this, UserDataActivity.class);
        startActivity(intent);
    }

    public void HomePage(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, UserDataActivity.class);
        startActivity(intent);
    }
}
