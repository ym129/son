package edu.duke.ece651.spring19.son.fitbittestapi.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderResult;
import edu.duke.ece651.spring19.son.fitbittestapi.R;
import edu.duke.ece651.spring19.son.fitbittestapi.databinding.LayoutInfoBinding;

/**
 * Created by SON Team.
 */

public abstract class InfoFragment<T> extends Fragment implements LoaderManager.LoaderCallbacks<ResourceLoaderResult<T>>, SwipeRefreshLayout.OnRefreshListener {
    protected LayoutInfoBinding binding;

    protected final String TAG = getClass().getSimpleName();
    protected HashMap map = new HashMap();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_info, container, false);

        binding.setTitleText(getTitleResourceId());
        setMainText(getActivity().getString(R.string.no_data));
        binding.swipeRefreshLayout.setOnRefreshListener(this);
        binding.setLoading(true);

        map.put("dateOfBirth", "Date of Birth");
        map.put("displayname", "Name");
        map.put("gender", "Gender");
        map.put("height", "Height(cm)");
        map.put("memberSince", "Join Fitbit At");
        map.put("weight", "Weight(kg)");
        map.put("caloriesOut", "Calories Consumed");
        map.put("steps", "Steps");
        map.put("sedentaryMinutes", "Time on Chair");
        map.put("fairlyActiveMinutes", "Time Fairly Active");
        map.put("lightlyActiveMinutes", "Time Lightly Active");
        map.put("veryActiveMinutes", "Time Extremely Active");
        map.put("strideLengthRunning", "Running Distance");
        map.put("strideLengthWalking", "Walking Distance");
        map.put("activeMinutes", "Time Active");
        map.put("distance", "Distance");
        map.put("avatar", "Avatar");

        return binding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(getLoaderId(), null, this).forceLoad();

    }

    @Override
    public void onLoadFinished(Loader<ResourceLoaderResult<T>> loader, ResourceLoaderResult<T> data) {
        binding.swipeRefreshLayout.setRefreshing(false);
        binding.setLoading(false);
        switch (data.getResultType()) {
            case ERROR:
                Toast.makeText(getActivity(), R.string.error_loading_data, Toast.LENGTH_LONG).show();
                break;
            case EXCEPTION:
                Log.e(TAG, "Error loading data", data.getException());
                Toast.makeText(getActivity(), R.string.error_loading_data, Toast.LENGTH_LONG).show();
                break;
        }
    }

    public abstract int getTitleResourceId();

    protected abstract int getLoaderId();

    @Override
    public void onLoaderReset(Loader<ResourceLoaderResult<T>> loader) {
        //no-op
    }


    @Override
    public void onRefresh() {
        getLoaderManager().getLoader(getLoaderId()).forceLoad();
    }


    private String formatNumber(Number number) {
        return NumberFormat.getNumberInstance(Locale.getDefault()).format(number);
    }

    private boolean isImageUrl(String string) {
        return (string.startsWith("http") &&
                (string.endsWith("jpg")
                        || string.endsWith("gif")
                        || string.endsWith("png")));
    }

    protected void printKeys(StringBuilder stringBuilder, Object object) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(new Gson().toJson(object));
            Iterator<String> keys = jsonObject.keys();
            stringBuilder.append("<table border='0'>");
            while (keys.hasNext()) {
                String key = keys.next();
                if(key.equals("avatar150")) continue;
                stringBuilder.append("<tr>");
                Object value = jsonObject.get(key);
                if (!(value instanceof JSONObject)
                        && !(value instanceof JSONArray)) {
//                    stringBuilder.append("<td><b>");
                    if(map.get(key) != null) {
                        if (map.get(key) != "Avatar") {
                            stringBuilder.append("<td><b>");
                            stringBuilder.append(map.get(key));
                            stringBuilder.append("</b></td>");
//                            stringBuilder.append("<td>");
                        }
                        if (value instanceof Number) {
                            stringBuilder.append("<td>");
                            stringBuilder.append(formatNumber((Number) value));
                            stringBuilder.append("</td>");
                        } else if (isImageUrl(value.toString())) {
                            stringBuilder.append("<center><img src=\"");
                            stringBuilder.append(value.toString());
                            stringBuilder.append("\" width=\"100\" height=\"100\"></center>");
                        } else {
                            stringBuilder.append("<td>");
                            stringBuilder.append(value.toString());
                            stringBuilder.append("</td>");
                        }
//                        stringBuilder.append("</td>");
                        stringBuilder.append("</tr>");
                    }
//                    else {
//                        stringBuilder.append(key);
//                    }
//                    stringBuilder.append("</b></td>");
//                    stringBuilder.append("<td>");
//                    if (value instanceof Number) {
//                        stringBuilder.append(formatNumber((Number) value));
//                    } else if (isImageUrl(value.toString())) {
//                        stringBuilder.append("<center><img src=\"");
//                        stringBuilder.append(value.toString());
//                        stringBuilder.append("\" width=\"100\" height=\"100\"></center>");
//                    } else {
//                        stringBuilder.append(value.toString());
//                    }
//                    stringBuilder.append("</td>");
//                    stringBuilder.append("</tr>");

                }
            }
            stringBuilder.append("</table>");
            System.out.println(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void setMainText(String text) {
        binding.webview.loadData(text, "text/html", "UTF-8");
    }

}
