package edu.duke.ece651.spring19.son.fitbittestapi.fragments;


import android.content.Loader;
import android.os.Bundle;

import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderResult;
import edu.duke.ece651.spring19.son.fitbitapi.models.User;
import edu.duke.ece651.spring19.son.fitbitapi.models.UserContainer;
import edu.duke.ece651.spring19.son.fitbitapi.services.UserService;
import edu.duke.ece651.spring19.son.fitbittestapi.R;


/**
 * Created by SON Team.
 */

public class ProfileFragment extends InfoFragment<UserContainer> {

    @Override
    public int getTitleResourceId() {
        return R.string.user_info;
    }

    @Override
    protected int getLoaderId() {
        return 1;
    }

    @Override
    public Loader<ResourceLoaderResult<UserContainer>> onCreateLoader(int id, Bundle args) {
        return UserService.getLoggedInUserLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<ResourceLoaderResult<UserContainer>> loader, ResourceLoaderResult<UserContainer> data) {
        super.onLoadFinished(loader, data);
        if (data.isSuccessful()) {
            bindProfileInfo(data.getResult().getUser());
        }
    }

    public void bindProfileInfo(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        printKeys(stringBuilder, user);
        setMainText(stringBuilder.toString());
    }


}
