package edu.duke.ece651.spring19.son.fitbittestapi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;


import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import edu.duke.ece651.spring19.son.fitbitauth.authentication.AuthenticationHandler;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.AuthenticationManager;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.AuthenticationResult;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;
import edu.duke.ece651.spring19.son.fitbittestapi.databinding.ActivityRootBinding;

public class RootActivity extends AppCompatActivity implements AuthenticationHandler {

    private ActivityRootBinding binding;
    public static Calendar c = Calendar.getInstance();
    public static AlarmManager alarmManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_root);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.setLoading(false);

        /**
         *  (Look in FitbitAuthApplication for Step 1)
         */


        /**
         *  2. If we are logged in, go to next activity
         *      Otherwise, display the login screen
         */
        if (AuthenticationManager.isLoggedIn()) {
            onLoggedIn();
        }
    }

    public void onLoggedIn() {
        Intent intent = UserDataActivity.newIntent(this);
        startActivity(intent);
        binding.setLoading(false);
        if (ReminderPage.min > 30) {
            RootActivity.c.set(Calendar.MINUTE, ReminderPage.min - 30);
            RootActivity.c.set(Calendar.HOUR_OF_DAY, ReminderPage.hour);
        }
        else {
            RootActivity.c.set(Calendar.HOUR_OF_DAY, ReminderPage.hour - 1);
            RootActivity.c.set(Calendar.MINUTE, 60 - 30 - ReminderPage.min);
        }
//        c.set(Calendar.HOUR_OF_DAY, ReminderPage.hour);
//        c.set(Calendar.MINUTE, ReminderPage.min);
        c.set(Calendar.SECOND, 0);
        Date date = new Date();
        int hours = date.getHours();
        int mins = date.getMinutes();
        if (hours < ReminderPage.hour || (hours == ReminderPage.hour && mins <= ReminderPage.min)){
            startAlarm(c);
        }
    }

    public void onLoginClick(View view) {
        binding.setLoading(true);
        /**
         *  3. Call login to show the login UI
         */
        AuthenticationManager.login(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /**
         *  4. When the Login UI finishes, it will invoke the `onActivityResult` of this activity.
         *  We call `AuthenticationManager.onActivityResult` and set ourselves as a login listener
         *  (via AuthenticationHandler) to check to see if this result was a login result. If the
         *  result code matches login, the AuthenticationManager will process the login request,
         *  and invoke our `onAuthFinished` method.
         *
         *  If the result code was not a login result code, then `onActivityResult` will return
         *  false, and we can handle other onActivityResult result codes.
         *
         */

        if (!AuthenticationManager.onActivityResult(requestCode, resultCode, data, this)) {
            // Handle other activity results, if needed
        }

    }

    public void onAuthFinished(AuthenticationResult authenticationResult) {
        binding.setLoading(false);

        /**
         * 5. Now we can parse the auth response! If the auth was successful, we can continue onto
         *      the next activity. Otherwise, we display a generic error message here
         */
        if (authenticationResult.isSuccessful()) {
            onLoggedIn();
        } else {
            displayAuthError(authenticationResult);
        }
    }

    private void displayAuthError(AuthenticationResult authenticationResult) {
        String message = "";

        switch (authenticationResult.getStatus()) {
            case dismissed:
                message = getString(R.string.login_dismissed);
                break;
            case error:
                message = authenticationResult.getErrorMessage();
                break;
            case missing_required_scopes:
                Set<Scope> missingScopes = authenticationResult.getMissingScopes();
                String missingScopesText = TextUtils.join(", ", missingScopes);
                message = getString(R.string.missing_scopes_error) + missingScopesText;
                break;
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.login_title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                .create()
                .show();
    }

    public void startAlarm(Calendar c){
        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertRecv.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,1,intent,0);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

}
