package edu.duke.ece651.spring19.son.fitbittestapi;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;



import java.util.ArrayList;
import java.util.List;

import edu.duke.ece651.spring19.son.fitbitauth.authentication.AuthenticationManager;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;
import edu.duke.ece651.spring19.son.fitbittestapi.fragments.ActivitiesFragment;
import edu.duke.ece651.spring19.son.fitbittestapi.fragments.ButtonFragment;
import edu.duke.ece651.spring19.son.fitbittestapi.fragments.DeviceFragment;
import edu.duke.ece651.spring19.son.fitbittestapi.fragments.InfoFragment;
import edu.duke.ece651.spring19.son.fitbittestapi.fragments.ProfileFragment;
import edu.duke.ece651.spring19.son.fitbittestapi.fragments.WeightLogFragment;

/**
 * Created by SON Team.
 */

public class UserDataPagerAdapter extends FragmentPagerAdapter {

    private final List<InfoFragment> fragments = new ArrayList<>();

    public UserDataPagerAdapter(FragmentManager fm) {
        super(fm);

        fragments.clear();
        if (containsScope(Scope.profile)) {
            fragments.add(new ProfileFragment());
        }
        if (containsScope(Scope.settings)) {
            fragments.add(new DeviceFragment());
        }
        if (containsScope(Scope.activity)) {
            fragments.add(new ActivitiesFragment());
        }
        if (containsScope(Scope.weight)) {
            fragments.add(new WeightLogFragment());
        }

        fragments.add(new ButtonFragment());

    }

    private boolean containsScope(Scope scope) {
        return AuthenticationManager.getCurrentAccessToken().getScopes().contains(scope);
    }

    @Override
    public Fragment getItem(int position) {
        if (position >= fragments.size()) {
            return null;
        }

        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public int getTitleResourceId(int index) {
        return fragments.get(index).getTitleResourceId();
    }
}
