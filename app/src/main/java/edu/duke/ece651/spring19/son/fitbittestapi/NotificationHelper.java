package edu.duke.ece651.spring19.son.fitbittestapi;

import android.content.ContextWrapper;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContextWrapper;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

public class NotificationHelper extends ContextWrapper {
    public static final String channel = "Survey";

    private NotificationManager mManager;
    public static PowerManager pm;
    public static KeyguardManager keyguardManager;

    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    public void createChannel() {
        NotificationChannel channel1 = new NotificationChannel(channel, "survey notification", NotificationManager.IMPORTANCE_DEFAULT);
        channel1.enableLights(true);

        getManager().createNotificationChannel(channel1);
    }

    public NotificationManager getManager() {
        if (mManager == null) {
            mManager  = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }

    public NotificationCompat.Builder getChannelNotification() {
        Intent resultIntent = new Intent(this, UserDataActivity.class);
        NotificationCompat.Builder mbuilder =  new NotificationCompat.Builder(this, channel)
                .setContentTitle("Notification")
                .setContentText("Time to finish the survey")
                .setSmallIcon(R.drawable.fitbit_logo)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setPriority(Notification.PRIORITY_DEFAULT)//设置该通知优先级
                .setCategory(Notification.CATEGORY_MESSAGE);//设置通知类别
        //.setColor(context.getResources().getColor(R.color.small_icon_bg_color))//设置smallIcon的背景色
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(UserDataActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mbuilder.setContentIntent(resultPendingIntent);
        wakeUpAndUnlock();
        return mbuilder;
    }

    public void wakeUpAndUnlock() {

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        // 屏幕解锁
        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
    }
}
