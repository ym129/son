package edu.duke.ece651.spring19.son.fitbitapi.exceptions;



import java.util.Collection;
import java.util.Set;

import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;

/**
 * Created by SON Team.
 */
public class MissingScopesException extends FitbitAPIException {

    private Collection<Scope> scopes;

    public MissingScopesException(Set<Scope> scopes) {
        this.scopes = scopes;
    }

    public Collection<Scope> getScopes() {
        return scopes;
    }
}
