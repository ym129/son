package edu.duke.ece651.spring19.son.fitbitapi.services;

import android.app.Activity;
import android.content.Loader;

import edu.duke.ece651.spring19.son.fitbitapi.exceptions.MissingScopesException;
import edu.duke.ece651.spring19.son.fitbitapi.exceptions.TokenExpiredException;
import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderFactory;
import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderResult;
import edu.duke.ece651.spring19.son.fitbitapi.models.UserContainer;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;


/**
 * Created by SON Team.
 */
public class UserService {

    private final static String USER_URL = "https://api.fitbit.com/1/user/-/profile.json";
    private static final ResourceLoaderFactory<UserContainer> USER_PROFILE_LOADER_FACTORY = new ResourceLoaderFactory<>(USER_URL, UserContainer.class);

    public static Loader<ResourceLoaderResult<UserContainer>> getLoggedInUserLoader(Activity activityContext) throws MissingScopesException, TokenExpiredException {
        return USER_PROFILE_LOADER_FACTORY.newResourceLoader(activityContext, new Scope[]{Scope.profile});
    }

}
