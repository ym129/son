package edu.duke.ece651.spring19.son.fitbitapi.services;

import android.app.Activity;
import android.content.Loader;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import edu.duke.ece651.spring19.son.fitbitapi.exceptions.MissingScopesException;
import edu.duke.ece651.spring19.son.fitbitapi.exceptions.TokenExpiredException;
import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderFactory;
import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderResult;
import edu.duke.ece651.spring19.son.fitbitapi.models.DailyActivitySummary;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;

/**
 * Created by SON Team.
 */
public class ActivityService {

    private final static String ACTIVITIES_URL = "https://api.fitbit.com/1/user/-/activities/date/%s.json";
    private static final ResourceLoaderFactory<DailyActivitySummary> USER_ACTIVITIES_LOADER_FACTORY = new ResourceLoaderFactory<>(ACTIVITIES_URL, DailyActivitySummary.class);
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    public static Loader<ResourceLoaderResult<DailyActivitySummary>> getDailyActivitySummaryLoader(Activity activityContext, Date date) throws MissingScopesException, TokenExpiredException {
        return USER_ACTIVITIES_LOADER_FACTORY.newResourceLoader(activityContext, new Scope[]{Scope.activity}, dateFormat.format(date));
    }

}
