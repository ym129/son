package edu.duke.ece651.spring19.son.fitbitapi.services;

import android.app.Activity;
import android.content.Loader;

import edu.duke.ece651.spring19.son.fitbitapi.exceptions.MissingScopesException;
import edu.duke.ece651.spring19.son.fitbitapi.exceptions.TokenExpiredException;
import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderFactory;
import edu.duke.ece651.spring19.son.fitbitapi.loaders.ResourceLoaderResult;
import edu.duke.ece651.spring19.son.fitbitapi.models.Device;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;


/**
 * Created by SON Team.
 */
public class DeviceService {

    private final static String DEVICE_URL = "https://api.fitbit.com/1/user/-/devices.json";
    private static final ResourceLoaderFactory<Device[]> USER_DEVICES_LOADER_FACTORY = new ResourceLoaderFactory<>(DEVICE_URL, Device[].class);

    public static Loader<ResourceLoaderResult<Device[]>> getUserDevicesLoader(Activity activityContext) throws MissingScopesException, TokenExpiredException {
        return USER_DEVICES_LOADER_FACTORY.newResourceLoader(activityContext, new Scope[]{Scope.settings});
    }

}
