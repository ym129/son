package edu.duke.ece651.spring19.son.fitbitapi.exceptions;

/**
 * Created by SON Team.
 */
public class FitbitAPIException extends RuntimeException {

    public FitbitAPIException() {
    }

    public FitbitAPIException(String message) {
        super(message);
    }
}
