package edu.duke.ece651.spring19.son.fitbitapi;

import android.app.Activity;



import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import edu.duke.ece651.spring19.son.fitbitapi.exceptions.MissingScopesException;
import edu.duke.ece651.spring19.son.fitbitapi.exceptions.TokenExpiredException;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.AccessToken;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.AuthenticationManager;
import edu.duke.ece651.spring19.son.fitbitauth.authentication.Scope;

/**
 * Created by SON Team.
 */
public class APIUtils {

    public static void validateToken(Activity contextActivity, AccessToken accessToken, Scope... scopes) throws MissingScopesException, TokenExpiredException {
        Set<Scope> requiredScopes = new HashSet<>(Arrays.asList(scopes));

        requiredScopes.removeAll(accessToken.getScopes());

        if (!requiredScopes.isEmpty()) {
            throw new MissingScopesException(requiredScopes);
        }

        if (accessToken.hasExpired()) {
            // Check to see if we should logout
            if (AuthenticationManager.getAuthenticationConfiguration().isLogoutOnAuthFailure()) {
                AuthenticationManager.logout(contextActivity);
            } else {
                throw new TokenExpiredException();
            }
        }
    }
}
